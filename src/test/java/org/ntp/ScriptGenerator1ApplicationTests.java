package org.ntp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.ntp.model.CodeTypeInvenRecord;
import org.ntp.model.Constants;
import org.ntp.model.InputCdTypeInven;
import org.ntp.model.JsonHeaders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ScriptGenerator1ApplicationTests {

	List<String> exludeList = Arrays.asList("CREATED_BY", "CREATED_DT", "LAST_UPDATED_BY", "LAST_UPDATED_DT", "VER_NO",
			"SERVER_NAME", "SYSTEM_NAME");

	final String filePath = "C:\\Users\\al-drin.g.san.pablo\\Documents\\01 Workspace\\STS\\ScriptGenerator-1\\src\\main\\resources\\";
	final String source1 = "INPUT_tbl_ccm_cd_type_inven.csv";
	final String target1 = "INSERT_tbl_ccm_cd_type_inven.xlsx";

	@Test
	public void generateCdTypeInvenScript() {

		ObjectMapper mapper = new ObjectMapper();
		List<CodeTypeInvenRecord> recordsList = new ArrayList<>();
		CodeTypeInvenRecord record = null;

		for (Map.Entry<String, List<InputCdTypeInven>> entry : CdTypeInvenScriptGenerator
				.groupByTableName(CdTypeInvenScriptGenerator.getObjectList(filePath + source1)).entrySet()) {

			InputCdTypeInven first = entry.getValue().get(0);
			record = new CodeTypeInvenRecord();
			record.setCodeTableName(first.getPhysicalTableName());
			record.setCodeTypeOwnerId(first.getModule());
			record.setCodeTypeMnemonic(first.getCodeTypeMnemonic());
			record.setCodeTypeViewInd(first.getViewIndicator());
			record.setCodeTypeAddInd(first.getAddIndicator());
			record.setCodeTypeUpdateInd(first.getUpdateIndicator());
			record.setCodeTypeWfInd(first.getWorkflowIndicator());

			try {
				List<String> headers = new ArrayList<>();
				for (InputCdTypeInven obj : entry.getValue()) {
					if (!obj.getPrimaryKey().equals(Constants.YES)
							&& !exludeList.contains(obj.getPhysicalColumnName())) {
						headers.add(obj.getPhysicalColumnName());
					}
				}
				record.setHeadersAsJson(mapper.writeValueAsString(new JsonHeaders(headers)));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			recordsList.add(record);
		}
		System.out.println("Records List Size: " + recordsList.size());

		try {
			CdTypeInvenScriptGenerator.writeToExcel(recordsList, filePath + target1);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
