package org.ntp;

import org.junit.Test;
import org.ntp.model.Constants;
import org.ntp.model.InputMetaData;
import org.ntp.model.MetadataRecord;
import org.ntp.service.MetaDataService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ScriptGenerator1ApplicationTest2 {


    @Test
    public void processMetadata(){
        List<InputMetaData> metadataInputlist = MetaDataService.readExcel();
        final String filePath = "C:\\\\Users\\\\meriel.s.a.bagaman\\\\Documents\\\\COMMON Script Gen" +
                "\\\\Java-TN-Common\\\\src\\\\main\\\\resources\\\\CCM_METADATA_INPUT.xlsx";

        for (InputMetaData inputMetaData : metadataInputlist) {
            MetadataRecord metadataRecord =  new MetadataRecord();
            Date todayDate = new Date();
            SimpleDateFormat dateformatter = new SimpleDateFormat(Constants.DATE_TIME_FORMATTER);
            String todayDateStr = dateformatter.format(todayDate);

            System.out.println("getTablename=" + inputMetaData.getTablename());
            System.out.println("getColumnname=" + inputMetaData.getColumnname());
            System.out.println("getDatatype=" + inputMetaData.getDatatype());
            System.out.println("getColumnLength=" + inputMetaData.getColumnLength());
            System.out.println("getIsNullable=" + inputMetaData.getIsNullable());
            System.out.println("getIsPrimaryKey=" + inputMetaData.getIsPrimaryKey());
            System.out.println("getJsonKey=" + inputMetaData.getJsonKey());

            System.out.println("getCreatedBy=" + metadataRecord.getCreatedBy());
            System.out.println("getCreatedDt=" + todayDateStr);
            System.out.println("getUpdatedBy=" + metadataRecord.getLastUpdBy());
            System.out.println("getUpdatedDt=" + metadataRecord.getLastUpdDt());
            System.out.println("getVerNo=" + metadataRecord.getVerNo());
            System.out.println("getSerName=" + metadataRecord.getServerName());
            System.out.println("getSysName=" + metadataRecord.getSysName());
        }


        //write here
        MetaDataService.writeToExcel(metadataInputlist, filePath);
    }

}
