package org.ntp.util;


public class Util {
	public static String setNotNull(String[] arr, int index) {
		try {
			String str = arr[index];
			return str != null && !"".equals(str) ? str.replaceAll("\"", "") : str;
		} catch (Exception e) {
			return "";
		}
	}
}
