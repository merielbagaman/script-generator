package org.ntp.model;

import org.ntp.util.Util;

public class InputMetaData {
	private String tablename;
	private String columnname;
	private String datatype;
	private String columnLength;
	private String isNullable;
	private String isPrimaryKey;
	private String jsonKey;
	private String typeInvenId;

	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getColumnname() {
		return columnname;
	}
	public void setColumnname(String columnname) {
		this.columnname = columnname;
	}
	public String getDatatype() {
		return datatype;
	}
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	public String getColumnLength() {
		return columnLength;
	}
	public void setColumnLength(String columnLength) {
		this.columnLength = columnLength;
	}
	public String getIsNullable() {
		return isNullable;
	}
	public void setIsNullable(String isNullable) {
		this.isNullable = isNullable;
	}
	public String getIsPrimaryKey() {
		return isPrimaryKey;
	}
	public void setIsPrimaryKey(String isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}
	public String getJsonKey() {
		return jsonKey;
	}
	public void setJsonKey(String jsonKey) {
		this.jsonKey = jsonKey;
	}
	public String getTypeInvenId() {
		return typeInvenId;
	}
	public void setTypeInvenId(String typeInvenId) {
		this.typeInvenId = typeInvenId;
	}

	public static InputMetaData create(String[] arr) {
		InputMetaData retObj = new InputMetaData();
		retObj.setTablename(Util.setNotNull(arr, 0));
		retObj.setColumnname(Util.setNotNull(arr, 1));
		retObj.setDatatype(Util.setNotNull(arr, 2));
		retObj.setColumnLength(Util.setNotNull(arr, 3));
		retObj.setIsNullable(Util.setNotNull(arr, 4));
		retObj.setIsPrimaryKey(Util.setNotNull(arr, 5));
		retObj.setJsonKey(Util.setNotNull(arr, 6));
		return retObj;
	}
}