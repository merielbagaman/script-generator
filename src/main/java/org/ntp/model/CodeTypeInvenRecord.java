package org.ntp.model;

public class CodeTypeInvenRecord {

	private String codeTypeCategory = Constants.CBC;
	private String codeTypeViewInd = Constants.N;
	private String codeTypeAddInd = Constants.N;
	private String codeTypeUpdateInd = Constants.N;
	private String codeTypeWfInd = Constants.N;
	
	private String codeTableName;
	private String codeTypeMnemonic;
	private String codeTypeOwnerId;
	private String headersAsJson;
	
	private String createdBy = Constants.SYSTEM;
	private String lastUpdateBy = Constants.SYSTEM;
	private String serverName = Constants.SERVER;
	private String systemName = Constants.SYSTEM;
	
	public String getCodeTypeCategory() {
		return codeTypeCategory;
	}
	public void setCodeTypeCategory(String codeTypeCategory) {
		this.codeTypeCategory = codeTypeCategory;
	}
	public String getCodeTypeViewInd() {
		return codeTypeViewInd;
	}
	public void setCodeTypeViewInd(String codeTypeViewInd) {
		this.codeTypeViewInd = codeTypeViewInd;
	}
	public String getCodeTypeAddInd() {
		return codeTypeAddInd;
	}
	public void setCodeTypeAddInd(String codeTypeAddInd) {
		this.codeTypeAddInd = codeTypeAddInd;
	}
	public String getCodeTypeUpdateInd() {
		return codeTypeUpdateInd;
	}
	public void setCodeTypeUpdateInd(String codeTypeUpdateInd) {
		this.codeTypeUpdateInd = codeTypeUpdateInd;
	}
	public String getCodeTypeWfInd() {
		return codeTypeWfInd;
	}
	public void setCodeTypeWfInd(String codeTypeWfInd) {
		this.codeTypeWfInd = codeTypeWfInd;
	}
	public String getCodeTableName() {
		return codeTableName;
	}
	public void setCodeTableName(String codeTableName) {
		this.codeTableName = codeTableName;
	}
	public String getCodeTypeMnemonic() {
		return codeTypeMnemonic;
	}
	public void setCodeTypeMnemonic(String codeTypeMnemonic) {
		this.codeTypeMnemonic = codeTypeMnemonic;
	}
	public String getCodeTypeOwnerId() {
		return codeTypeOwnerId;
	}
	public void setCodeTypeOwnerId(String codeTypeOwnerId) {
		this.codeTypeOwnerId = codeTypeOwnerId;
	}
	public String getHeadersAsJson() {
		return headersAsJson;
	}
	public void setHeadersAsJson(String headersAsJson) {
		this.headersAsJson = headersAsJson;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getLastUpdateBy() {
		return lastUpdateBy;
	}
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
}
