package org.ntp.model;

import org.ntp.util.Util;

import java.text.SimpleDateFormat;

public class MetadataRecord {

        private String createdBy = Constants.CAV;
        private SimpleDateFormat   createdDt = new SimpleDateFormat();
        private String lastUpdBy;
        private String lastUpdDt;
        private String verNo = Constants.ONE;
        private String serverName = Constants.SERVER;
        private String sysName = Constants.SYSTEM;

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public SimpleDateFormat getCreatedDt() {
            return createdDt;
        }

        public void setCreatedDt(SimpleDateFormat createdDt) {
            this.createdDt = createdDt;
        }

        public String getLastUpdBy() {
            return lastUpdBy;
        }

        public void setLastUpdBy(String lastUpdBy) {
            this.lastUpdBy = lastUpdBy;
        }

        public String getLastUpdDt() {
            return lastUpdDt;
        }

        public void setLastUpdDt(String lastUpdDt) {
            this.lastUpdDt = lastUpdDt;
        }

        public String getVerNo() {
            return verNo;
        }

        public void setVerNo(String verNo) {
            this.verNo = verNo;
        }

        public String getServerName() {
            return serverName;
        }

        public void setServerName(String serverName) {
            this.serverName = serverName;
        }

        public String getSysName() {
            return sysName;
        }

        public void setSysName(String sysName) {
            this.sysName = sysName;
        }

        public MetadataRecord create(String[] arr) {
            MetadataRecord retObj = new MetadataRecord();
            retObj.setCreatedBy(Util.setNotNull(arr, 0));
            retObj.setCreatedDt(getCreatedDt());
            retObj.setLastUpdBy(Util.setNotNull(arr, 2));
            retObj.setLastUpdDt(Util.setNotNull(arr, 3));
            retObj.setVerNo(Util.setNotNull(arr, 4));
            retObj.setServerName(Util.setNotNull(arr, 5));
            retObj.setSysName(Util.setNotNull(arr, 6));
            return retObj;
    }
}
