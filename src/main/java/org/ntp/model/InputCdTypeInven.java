package org.ntp.model;

public class InputCdTypeInven {

	//custom
	private String codeTypeMnemonic;
	private String viewIndicator;
	private String addIndicator;
	private String updateIndicator;
	private String workflowIndicator;
	
	//FROM PDM
	private String module;
	private String logicalDataEntity;
	private String physicalTableName;
	private String logicalColumnName;
	private String physicalColumnName;
	private String columnDataType;
	private String columnNullOption;
	private String primaryKey;
	private String foreignKey;
	private String length;
	private String minLength;

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getLogicalDataEntity() {
		return logicalDataEntity;
	}

	public void setLogicalDataEntity(String logicalDataEntity) {
		this.logicalDataEntity = logicalDataEntity;
	}

	public String getPhysicalTableName() {
		return physicalTableName;
	}

	public void setPhysicalTableName(String physicalTableName) {
		this.physicalTableName = physicalTableName;
	}

	public String getLogicalColumnName() {
		return logicalColumnName;
	}

	public void setLogicalColumnName(String logicalColumnName) {
		this.logicalColumnName = logicalColumnName;
	}

	public String getPhysicalColumnName() {
		return physicalColumnName;
	}

	public void setPhysicalColumnName(String physicalColumnName) {
		this.physicalColumnName = physicalColumnName;
	}

	public String getColumnDataType() {
		return columnDataType;
	}

	public void setColumnDataType(String columnDataType) {
		this.columnDataType = columnDataType;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getColumnNullOption() {
		return columnNullOption;
	}

	public void setColumnNullOption(String columnNullOption) {
		this.columnNullOption = columnNullOption;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getForeignKey() {
		return foreignKey;
	}

	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}

	public static InputCdTypeInven create(String[] arr) {
		InputCdTypeInven retObj = new InputCdTypeInven();
		retObj.setCodeTypeMnemonic(setNotNull(arr, 0));
		retObj.setViewIndicator(setNotNull(arr, 1));
		retObj.setAddIndicator(setNotNull(arr, 2));
		retObj.setUpdateIndicator(setNotNull(arr, 3));
		retObj.setWorkflowIndicator(setNotNull(arr, 4));
		
		//starting index for PDM entries
		int startIndex = 4;
		retObj.setModule(setNotNull(arr, startIndex+1));
		retObj.setLogicalDataEntity(setNotNull(arr, startIndex+2));
		retObj.setPhysicalTableName(setNotNull(arr, startIndex+3));
		retObj.setLogicalColumnName(setNotNull(arr, startIndex+4));
		retObj.setPhysicalColumnName(setNotNull(arr, startIndex+5));
		retObj.setColumnDataType(setNotNull(arr, startIndex+6));
		retObj.setColumnNullOption(setNotNull(arr, startIndex+7));
		retObj.setPrimaryKey(setNotNull(arr, startIndex+8));
		retObj.setForeignKey(setNotNull(arr, startIndex+9));
		retObj.setLength(setNotNull(arr, startIndex+10));
		retObj.setMinLength(setNotNull(arr, startIndex+11));
		return retObj;
	}

	public static String setNotNull(String[] arr, int index) {
		try {
			String str = arr[index];
			return str != null && !"".equals(str) ? str.replaceAll("\"", "") : str;
		} catch (Exception e) {
			return "";
		}
	}

	public String getMinLength() {
		return minLength;
	}

	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}

	public String getCodeTypeMnemonic() {
		return codeTypeMnemonic;
	}

	public void setCodeTypeMnemonic(String codeTypeMnemonic) {
		this.codeTypeMnemonic = codeTypeMnemonic;
	}

	public String getViewIndicator() {
		return viewIndicator;
	}

	public void setViewIndicator(String viewIndicator) {
		this.viewIndicator = viewIndicator;
	}

	public String getAddIndicator() {
		return addIndicator;
	}

	public void setAddIndicator(String addIndicator) {
		this.addIndicator = addIndicator;
	}

	public String getUpdateIndicator() {
		return updateIndicator;
	}

	public void setUpdateIndicator(String updateIndicator) {
		this.updateIndicator = updateIndicator;
	}

	public String getWorkflowIndicator() {
		return workflowIndicator;
	}

	public void setWorkflowIndicator(String workflowIndicator) {
		this.workflowIndicator = workflowIndicator;
	}
	
}
