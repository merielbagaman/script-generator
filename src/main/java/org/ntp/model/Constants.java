package org.ntp.model;

public class Constants {

	public static final String SERVER = "SERVER";
	public static final String SYSTEM = "SYSTEM";
	public static final String N = "N";
	public static final String CBC = "CBC";
	public static final String YES = "Yes";
	public static final String CAV = "CAV";

	public static final String ONE = "1";
    public static final String DATE_TIME_FORMATTER = "dd-MM-yyyy HH:mm:ss";


	public static final String CD_TYP_INVEN = "NTP_COMMON.TBL_CCM_CD_TYPE_INVEN";
	public static final String[] CD_TYPE_INVEN_COL = { "CCM_CD_CATEGORY", "CCM_CD_TYPE_OWNER_ID", "CCM_CD_TYPE_NAME",
			"CCM_CD_TYPE_MNEMONIC", "CCM_CD_TYPE_XSD_TEMPLATE_HDRS", "CCM_CD_TYPE_VIEW_IND", "CCM_CD_TYPE_ADD_IND",
			"CCM_CD_TYPE_UPDATE_IND", "CCM_CD_TYPE_WF_IND", "CREATED_BY", "LAST_UPDATED_BY", "SERVER_NAME",
			"SYSTEM_NAME" };

	public static String[] CMN_CCM_MDATA = {"CREATED_BY",
			"CREATED_DT","LAST_UPDATED_BY", "LAST_UPDATED_DT","VER_NO","SERVER_NAME","SYSTEM_NAME"
			,"CCM_TABLE_NAME","CCM_COLUMN_NAME","CCM_DATA_TYPE","CCM_COLUMN_LENGTH",
			"CCM_IS_NULLABLE","CCM_IS_PRIMARY_KEY","CCM_JSON_KEY"};
}
