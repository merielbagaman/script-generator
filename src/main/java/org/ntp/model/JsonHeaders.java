package org.ntp.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonHeaders {

	public JsonHeaders(List<String> list) {
		headers = list;
	}
	
	@JsonProperty("HEADERS")
	private List<String> headers;

	public List<String> getHeaders() {
		return headers;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	
}
