package org.ntp;

import org.ntp.model.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
public class ScriptGenerator1Application {

	public static void main(String[] args) {
		SpringApplication.run(ScriptGenerator1Application.class, args);

		Date todayDate = new Date();
		SimpleDateFormat dateformatter = new SimpleDateFormat(Constants.DATE_TIME_FORMATTER);
		String todayDateStr = dateformatter.format(todayDate);
		System.out.println("Today's date into dd-MM-yyyy format: " + todayDateStr);
	}
}
