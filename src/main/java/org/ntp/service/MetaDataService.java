package org.ntp.service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.ntp.model.*;

public class MetaDataService {

	static final String INPUT_XLSX_FILE_PATH = "C:\\\\Users\\\\meriel.s.a.bagaman\\\\Documents\\\\COMMON Script Gen" +
			"\\\\Java-TN-Common\\\\src\\\\main\\\\resources\\\\CCM_SOLUTION.xlsx";
	final String target1 = "INSERT_tbl_ccm_metadata.xlsx";
	
	public static List<InputMetaData> readExcel() {
		List<InputMetaData> metadataInputlist = new ArrayList<InputMetaData>();
		try {
			FileInputStream excelFile = new FileInputStream(new File(INPUT_XLSX_FILE_PATH));
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet sheet = workbook.getSheet("TBL_CMN_CCM_METADATA");
			
			DataFormatter dataFormatter = new DataFormatter();
			
	        sheet.forEach(row -> {
	        	InputMetaData newInput = new InputMetaData();
	            row.forEach(cell -> {
	                String cellValue = dataFormatter.formatCellValue(cell);
	                switch (cell.getColumnIndex()) {
						case 0:
							newInput.setTablename(cellValue);
							break;
						case 1:
							newInput.setColumnname(cellValue);
							break;
						case 2:
							newInput.setDatatype(cellValue);
							break;
						case 3:
							newInput.setColumnLength(cellValue);
							break;
						case 4:
							newInput.setIsNullable(cellValue);
							break;
						case 5:
							newInput.setIsPrimaryKey(cellValue);
							break;
						case 6:
							newInput.setJsonKey(cellValue);
							break;
					}
	            });
	        	metadataInputlist.add(newInput);
	        });
/*			workbook.close();
			excelFile.close();*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return metadataInputlist;
	}

	public static void writeToExcel(List<InputMetaData> recordsList, String excelPath)  {

		//System.out.println("INITIATE write to excel sheet:" + excelPath);

		File excelFile = null;
		XSSFWorkbook workbook = null;
		FileOutputStream fileOut = null;
		Row row = null;
		Cell cell = null;

		try {
			excelFile = new File(excelPath);
			workbook = new XSSFWorkbook();
			Sheet sheet = workbook.createSheet("TBL_CMN_CCM_METADATA");
			Sheet sheet2 = workbook.createSheet("INSERT_SCRIPT");

			/* Row 0 - Header START */
			row = sheet.createRow(0);
			for (int x = 0; x < Constants.CMN_CCM_MDATA.length; x++) {
				cell = row.createCell(x);
				cell.setCellType(CellType.STRING);
				cell.setCellValue(Constants.CMN_CCM_MDATA[x]);
			}
			/* Row 0 - Header END */

			// Iterate from Row 1 onwards
			int rowCount = 1;

			for (InputMetaData record : recordsList) {
				if (record.getTablename().equals("TABLE_NAME") && record.getColumnname().equals("COLUMN_NAME")) continue;
				row = sheet.createRow(rowCount);
				MetadataRecord metRec = new MetadataRecord();

                Date todayDate = new Date();
                SimpleDateFormat dateFormatter = new SimpleDateFormat(Constants.DATE_TIME_FORMATTER);
                String todayDateStr = dateFormatter.format(todayDate);

				for (int x = 0; x < Constants.CMN_CCM_MDATA.length; x++) {
					cell = row.createCell(x);
					cell.setCellType(CellType.STRING);

					switch (x) {
						case 0:
							cell.setCellValue(metRec.getCreatedBy());
							break;
						case 1:
							cell.setCellValue(todayDateStr);
							break;
						case 2:
							cell.setCellValue(metRec.getLastUpdBy());
							break;
						case 3:
							cell.setCellValue(metRec.getLastUpdDt());
							break;
						case 4:
							cell.setCellValue(metRec.getVerNo());
							break;
						case 5:
							cell.setCellValue(metRec.getServerName());
							break;
						case 6:
							cell.setCellValue(metRec.getSysName());
							break;
						case 7:
							cell.setCellValue("NTP_" + metRec.getCreatedBy() + "_" + record.getTablename());
							break;
						case 8:
							cell.setCellValue(record.getColumnname());
							break;
						case 9:
							cell.setCellValue(record.getDatatype());
							break;
						case 10:
							cell.setCellValue(record.getColumnLength());
							break;
						case 11:
							cell.setCellValue(record.getIsNullable());
							break;
						case 12:
							cell.setCellValue(record.getIsPrimaryKey());
							break;
						case 13:
							cell.setCellValue(record.getColumnname().toLowerCase().replaceAll("[_]",""));
							break;
						default:
							break;
					}
				}

                 /*   if (!record.getCodeTypeMnemonic().equals("0")) {
                        Row row2 = sheet2.createRow(rowCount);
                        Cell cell2 = row2.createCell(0);
                        cell2.setCellType(CellType.STRING);
                        cell2.setCellValue(org.ntp.CdTypeInvenScriptGenerator.getInsertRecord(record));
                    }*/

				// !IMPT
				rowCount++;
				row = null;
				cell = null;
			}
			for(int i = 0; i < Constants.CMN_CCM_MDATA.length; i++) {
				sheet.autoSizeColumn(i);
			}

			fileOut = new FileOutputStream(excelFile);
			workbook.write(fileOut);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} /*finally {
			workbook.close();
			if (null != fileOut) {
				fileOut.close();
			}*/
		//}
		//System.out.println("COMPLETED Writing excel sheet: " + excelPath);
	}
	
	public static void updateDatabase() {
		
	}
	
	public static void insertDatabase() {
		
	}
	
	private static void printCellValue(Cell cell) {
	    switch (cell.getCellTypeEnum()) {
	        case BOOLEAN:
	            System.out.print(cell.getBooleanCellValue());
	            break;
	        case STRING:
	            System.out.print(cell.getRichStringCellValue().getString());
	            break;
	        case NUMERIC:
	            if (DateUtil.isCellDateFormatted(cell)) {
	                System.out.print(cell.getDateCellValue());
	            } else {
	                System.out.print(cell.getNumericCellValue());
	            }
	            break;
	        case FORMULA:
	            System.out.print(cell.getCellFormula());
	            break;
	        case BLANK:
	            System.out.print("");
	            break;
	        default:
	            System.out.print("");
	    }

	    System.out.print("\t");
	}
}