package org.ntp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.ntp.model.CodeTypeInvenRecord;
import org.ntp.model.Constants;
import org.ntp.model.InputCdTypeInven;

public abstract class CdTypeInvenScriptGenerator {

	public static List<InputCdTypeInven> getObjectList(String filePath) {
		System.out.println("Retrieving CSV...");
		List<InputCdTypeInven> list = new ArrayList<>();
		try {
			BufferedReader br = Files.newBufferedReader(Paths.get(filePath));

			String line = br.readLine();
			boolean isFirstHeaders = true;
			while (line != null) {
				if (isFirstHeaders) {
					isFirstHeaders = false;
					// System.out.println("HEADERS: " + line);
					line = br.readLine();
				}
				String[] attributes = line.split(",");
				// System.out.println(line);
				list.add(InputCdTypeInven.create(attributes));
				line = br.readLine();
			}
			System.out.println("LIST SIZE: " + list.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static Map<String, List<InputCdTypeInven>> groupByTableName(List<InputCdTypeInven> list) {
		Map<String, List<InputCdTypeInven>> map = new HashMap<>();
		try {
			for (InputCdTypeInven obj : list) {
				if (!map.containsKey(obj.getPhysicalTableName())) {
					map.put(obj.getPhysicalTableName(), new ArrayList<>());
				}
				map.get(obj.getPhysicalTableName()).add(obj);
			}

			System.out.println("MAP SIZE: " + map.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public static void writeToExcel(List<CodeTypeInvenRecord> recordsList, String excelPath) throws IOException {
		System.out.println("INITIATE write to excel sheet:" + excelPath);

		File excelFile = null;
		XSSFWorkbook workbook = null;
		FileOutputStream fileOut = null;
		Row row = null;
		Cell cell = null;

		try {
			excelFile = new File(excelPath);
			workbook = new XSSFWorkbook();
			Sheet sheet = workbook.createSheet("TBL_CCM_CD_TYPE_INVEN");
			Sheet sheet2 = workbook.createSheet("INSERT_SCRIPT");

			/* Row 0 - Header START */
			row = sheet.createRow(0);
			String[] columns = { "CCM_CD_CATEGORY", "CCM_CD_TYPE_OWNER_ID", "CCM_CD_TYPE_NAME", "CCM_CD_TYPE_MNEMONIC",
					"CCM_CD_TYPE_VIEW_IND", "CCM_CD_TYPE_ADD_IND", "CCM_CD_TYPE_UPDATE_IND", "CCM_CD_TYPE_WF_IND",
					"HDRS_LENGTH", "CCM_CD_TYPE_XSD_TEMPLATE_HDRS" };
			for (int x = 0; x < columns.length; x++) {
				cell = row.createCell(x);
				cell.setCellType(CellType.STRING);
				cell.setCellValue(columns[x]);
			}
			/* Row 0 - Header END */

			// Iterate from Row 1 onwards
			int rowCount = 1;

			for (CodeTypeInvenRecord record : recordsList) {
				row = sheet.createRow(rowCount);

				for (int x = 0; x < columns.length; x++) {
					cell = row.createCell(x);
					cell.setCellType(CellType.STRING);

					switch (x) {
					case 0:
						cell.setCellValue(record.getCodeTypeCategory());
						break;
					case 1:
						cell.setCellValue(record.getCodeTypeOwnerId());
						break;
					case 2:
						cell.setCellValue(record.getCodeTableName());
						break;
					case 3:
						cell.setCellValue(record.getCodeTypeMnemonic());
						break;
					case 4:
						cell.setCellValue(record.getCodeTypeViewInd());
						break;
					case 5:
						cell.setCellValue(record.getCodeTypeAddInd());
						break;
					case 6:
						cell.setCellValue(record.getCodeTypeUpdateInd());
						break;
					case 7:
						cell.setCellValue(record.getCodeTypeWfInd());
						break;
					case 8:
						cell.setCellValue(record.getHeadersAsJson().length());
						break;
					case 9:
						cell.setCellValue(record.getHeadersAsJson());
						break;
					default:
						break;
					}
				}

				if (!record.getCodeTypeMnemonic().equals("0")) {
					Row row2 = sheet2.createRow(rowCount);
					Cell cell2 = row2.createCell(0);
					cell2.setCellType(CellType.STRING);
					cell2.setCellValue(CdTypeInvenScriptGenerator.getInsertRecord(record));
				}

				// !IMPT
				rowCount++;
				row = null;
				cell = null;
			}

			fileOut = new FileOutputStream(excelFile);
			workbook.write(fileOut);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			workbook.close();
			if (null != fileOut) {
				fileOut.close();
			}
		}
		System.out.println("COMPLETED Writing excel sheet: " + excelPath);
	}

	public static String getInsertRecord(CodeTypeInvenRecord record) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ").append(Constants.CD_TYP_INVEN).append("(");
		sb.append(String.join(",", Constants.CD_TYPE_INVEN_COL));
		sb.append(") VALUES (\'");
		sb.append(record.getCodeTypeCategory()).append("\',\'");
		sb.append(record.getCodeTypeOwnerId()).append("\',\'");
		sb.append(record.getCodeTableName()).append("\',\'");
		sb.append(record.getCodeTypeMnemonic()).append("\',");
		sb.append("q\'<").append(record.getHeadersAsJson()).append(">\',\'");
		sb.append(record.getCodeTypeViewInd()).append("\',\'");
		sb.append(record.getCodeTypeAddInd()).append("\',\'");
		sb.append(record.getCodeTypeUpdateInd()).append("\',\'");
		sb.append(record.getCodeTypeWfInd()).append("\',\'");
		sb.append(record.getCreatedBy()).append("\',\'");
		sb.append(record.getLastUpdateBy()).append("\',\'");
		sb.append(record.getServerName()).append("\',\'").append(record.getSystemName()).append("\');");
		return sb.toString();
	}

}
